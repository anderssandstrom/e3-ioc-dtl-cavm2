##############################################################################
############ Startup File for DTL Cavities Master2
############
############ Authors: Maurizio Montis || INFN-LNL
############          Alfio Rizzo || ESS
############ Mail:   maurizio.montis@lnl.infn.it
############         alfio.rizzo@ess..eu
##############################################################################

require dtlcavm2
require std
require essioc
require ecmccfg

# Register our local db directory
epicsEnvSet(DB_DIR, "$(E3_CMD_TOP)/db/")

# Register our local iocsh directory
epicsEnvSet(IOCSH_DIR, "$(E3_CMD_TOP)/iocsh/")

# Register our local axis config directory
epicsEnvSet(AXIS_DIR, "$(E3_CMD_TOP)/axiscfg/")

# Ethercat Master Name and Index
epicsEnvSet("MASTERNAME", "DTL:Ctrl-ECAT-002")
epicsEnvSet("MASTERID","3")


# Load Common EPICS Modules
iocshLoad("$(essioc_DIR)common_config.iocsh")

# Configure Master2 Slaves
iocshLoad("$(IOCSH_DIR)dtl-ethercat-m2-cfg.iocsh")

# Map ecmccfg Digital IO PVs
iocshLoad("$(IOCSH_DIR)dtl-digitalIO-pv-map.iocsh")

# Map ecmccfg Linear Encoder PVs
iocshLoad("$(IOCSH_DIR)dtl-encoder-pv-map.iocsh")

# Map ecmccfg Motor Driver PVs
iocshLoad("$(IOCSH_DIR)dtl-motor-driver-pv-map.iocsh")

##############################################################################
################ Post Configuration - Load Additional Databases

# General controls
dbLoadRecords("$(DB_DIR)dtl-tuner-control.db")
dbLoadRecords("$(DB_DIR)dtl-tuner-totalok-status.db")
dbLoadRecords("$(DB_DIR)dtl-arcdetect-control.db")
dbLoadRecords("$(DB_DIR)dtl-tuner-freq-error.db") # Fix the temporary records !!!

# ecmc Driver Reset Commands
iocshLoad("$(IOCSH_DIR)dtl-driver-reset-cmd.iocsh")

# Axis Disable Conditions
iocshLoad("$(IOCSH_DIR)dtl-tuner-disable-cond.iocsh")

# PID Tuner Controls
iocshLoad("$(IOCSH_DIR)dtl-tuner-pid-ctrl.iocsh") # Fix Temporary Input and Output PVs !!!

# Enable Reset System
iocshLoad("$(IOCSH_DIR)dtl-tuner-enable-reset.iocsh")

# Axis Enable System
iocshLoad("$(IOCSH_DIR)dtl-axis-postinit.iocsh")

# Arc Detection Ok Status
iocshLoad("$(IOCSH_DIR)dtl-arc-okstate.iocsh") # Fix temporary Input and Output PVs !!!

# Tuners Tanks Ok Status
iocshLoad("$(IOCSH_DIR)dtl-tuner-tank-okstatus.iocsh")

# Tuner System - Maintenance Reset
dbLoadRecords("$(DB_DIR)dtl-tuner-reset-maintenance.db")

# Arc Detection System - Maintenance Reset
dbLoadRecords("$(DB_DIR)dtl-arcdetect-reset-maintenance.db")

## SQL State Machines
# Tuner System/Arc Detection System - State Machine code
iocshLoad("$(dtlcavm2_DIR)dtl-statemachine-code-pv.iocsh")
iocshLoad("$(dtlcavm2_DIR)postInitStateMachine-cfg.iocsh")

